/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

// main file parameters
pub const NXM: usize = 256;
pub const NKXM: usize = 512;

// definition of constants
pub const Q: f64 = 1.60217733e-19;    // Electron charge in absolute value (Coulomb)
pub const HBAR: f64 = 1.05457266e-34; // Reduced Planck constant (Joule*sec)
pub const ME: f64 = 9.1093897e-31;     // free electron mass (Kg)
pub const MP: f64 = 1.6726217e-27;      // free proton mass (Kg)
pub use std::f64::consts::PI;           // Pi number
pub const EPS0: f64 = 8.854187817e-12;         // Permittivity of free space (F/m)

// Bohr radius
pub const A0: f64 = 4.*PI*EPS0*HBAR*HBAR/(ME*Q*Q);

// define device properties
pub const LX: f64 = 8.*A0;          // total lenght of device
pub const LC: f64 = LX/3.;          // coherence lenght
pub const NX: usize = 120;          // number of cells in x-direction
pub const DT: f64 = 0.01e-18;       // time step
pub const ITMAX: i64 = 1200;        // total number of time steps

// Electron spread over the domain at rest
pub const SIGMA_WAVE_PACKET_ELEC: f64 = A0/3.;      // wave packet width
pub const X0_WAVE_PACKET_ELEC: f64 = 0.5*LX;        // wave packet initial position

// Proton in the middle at rest
pub const SIGMA_WAVE_PACKET_PROT: f64 = A0/12.;      // wave packet width
pub const X0_WAVE_PACKET_PROT: f64 = 0.5*LX;        // wave packet initial position

// spatial cell lenght
pub const DX: f64 = LX/(NX as f64);

// automatic calculation of NKX
pub const NKX: isize = (0.5*LC/DX) as isize;

// pseudo-wave vector lenght
pub const DKX: f64 = PI/LC;

// electron and proton at rest
pub const K0_WAVE_PACKET_ELEC: f64 = 0.*DKX;        // wave packet initial wave vector
pub const K0_WAVE_PACKET_PROT: f64 = 0.*DKX;        // wave packet initial wave vector

// variable packing
pub const X0_WAVE_PACKET: [f64;2] = [X0_WAVE_PACKET_ELEC, X0_WAVE_PACKET_PROT];
pub const SIGMA_WAVE_PACKET: [f64;2] = [SIGMA_WAVE_PACKET_ELEC, SIGMA_WAVE_PACKET_PROT];
pub const K0_WAVE_PACKET: [f64;2] = [K0_WAVE_PACKET_ELEC, K0_WAVE_PACKET_PROT];
