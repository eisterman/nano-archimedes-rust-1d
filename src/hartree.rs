/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{ArrayView1, ArrayView2, ArrayViewMut1};

use crate::config::*;

// calculates the Hartree potentials due to the n-particles

pub fn hartree(rho: ArrayView2<f64>,
               mut phi: ArrayViewMut1<f64>,
               phi0: ArrayView1<f64>) {
    println!("\ncalculating the Hartree potential");

    for i in 1_usize..=NX {
        let mut actual_rho = 0.;
        let mut sum = 0.;
        // calculation of the electron(s) potential
        {
            let mut r = 0.;
            for ii in 1_usize..=NX {
                if ii != i {
                    r = ((i as f64 - ii as f64)*DX).abs();
                    for n in 0_usize..=1 {
                        sum += rho[(n,ii)] / r;
                    }
                }
            }
        }
        sum *= DX;
        for n in 0_usize..=1 {
            actual_rho += rho[(n,i)]; // calculation of the actual total density
        }
        phi[i] = phi0[i] - 0.25*Q/(PI*EPS0)*sum; // potential barrier + hartree potential
    }
}
