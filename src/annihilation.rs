/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::io::{self,Write};
use rand::{Rng,thread_rng};

use ndarray::{Array,ArrayViewMut1,ArrayViewMut2,ArrayViewMut4};
use ndarray::{s,Zip};
use ndarray_parallel::prelude::*;

use crate::config::*;
use num_traits::real::Real;

// annihilates all unnecessary particles according to the
// previously calculated distribution function dist[( , )]

pub fn annihilation<T: Rng>(inum: &mut i64,
                    mut rng: &mut T,
                    dist: ArrayViewMut4<i64>,
                    mut p: ArrayViewMut2<f64>,
                    mut k: ArrayViewMut2<i64>,
                    mut w: ArrayViewMut1<i64>) {
    println!("\n# of particles before annihilation = {0}", inum);
    io::stdout().flush().unwrap();

    // calculates the new array of particles
    *inum = 0;
    for i in 1_usize..=NX {
        for j in 1_usize..=NX {
            for m in 0_usize..(2*NKX-1) as usize {
                for n in 0_usize..(2*NKX-1) as usize {
                    let local_number_of_particles: i64 = dist[(i,j,m,n)].abs();
                    // creates the new local particles in the (i,j,m,n)-th phase-space cell
                    // the particles are uniformely distributed in space
                    for l in 1..=local_number_of_particles as usize {
                        let ind_k = *inum as usize + l;
                        //TODO: Migliorabile con l'uso dei Zip, forse anche parallelizzabile
                        if rng.gen::<f64>() > 0.5 {
                            p[(0,ind_k)] = (i as f64 - 0.5 + 0.5 * rng.gen::<f64>())*DX;
                        } else {
                            p[(0,ind_k)] = (i as f64 - 0.5 - 0.5 * rng.gen::<f64>())*DX;
                        }
                        if rng.gen::<f64>() > 0.5 {
                            p[(1,ind_k)] = (j as f64 - 0.5 + 0.5 * rng.gen::<f64>())*DX;
                        } else {
                            p[(1,ind_k)] = (j as f64 - 0.5 - 0.5 * rng.gen::<f64>())*DX;
                        }
                        k[(0,ind_k)] = m as i64 - NKX as i64 + 1;
                        k[(1,ind_k)] = n as i64 - NKX as i64 + 1;
                        if dist[(i,j,m,n)] > 0 {
                            w[ind_k] = 1;
                        } else {
                            w[ind_k] = -1;
                        }
                    }
                    *inum += local_number_of_particles;
                }
            }
        }
    }
    println!("# of particles after annihilation  = {0}\n", *inum);
    io::stdout().flush().unwrap();
}