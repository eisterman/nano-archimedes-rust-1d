/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::fs::File;
use std::fs::OpenOptions;
use std::io;
use std::io::prelude::*;
use std::fmt::write;

use ndarray::{Array2, ArrayView1, ArrayView2, ArrayView4, ArrayViewMut1};

use crate::config::*;

pub fn save(ind: i64, phi: ArrayView1<f64>,
            gamma: ArrayView2<f64>,
            dist: ArrayView4<i64>,
            rho: ArrayView2<f64>,
            mut rho0: ArrayViewMut1<f64>) -> std::io::Result<()> {
    println!("\nsaving output files = {}..", ind);
    io::stdout().flush().unwrap();

    if ind == 0 {
        // saves potential
        let mut fp = File::create("out/potential.dat")?;
        for i in 1_usize..=NX {
            writeln!(fp, "{} {}", (i as f64 - 0.5)*DX, phi[i])?;
        }

        // saves auxiliary files
        fp = File::create("out/x.dat")?;
        for i in 1_usize..=NX {
            writeln!(fp, "{}", (i as f64 - 0.5)*DX*1.0_e9)?; // in nanometer
        }

        fp = File::create("out/k.dat")?;
        for i in (-NKX+1)..NKX {
            writeln!(fp, "{}", i as f64 * DKX*1.0_e9)?; // in 1/nm
        }
    }

    // saves gamma function
    let mut fp = File::create(format!("out/gamma_{}.dat", ind))?;
    for i in 1_usize..=NX {
        for j in 1_usize..=NX {
            write!(fp, "{} ", gamma[(i,j)])?;
        }
        writeln!(fp)?;
    }

    // saves the distribution in 2-space
    fp = File::create(format!("out/distribution_x_{}.dat",ind))?;
    for i in 1_usize..=NX {
        for j in 1_usize..=NX {
            let mut sum = 0;
            for m in 0_usize..(2 * NKX as usize) {
                for n in 0_usize..(2 * NKX as usize) {
                    sum += dist[(i,j,m,n)];
                }
            }
            write!(fp,"{} ",sum)?;
        }
        writeln!(fp)?;
    }

    // saves the distribution in 2-wave space
    fp = File::create(format!("out/distribution_k_{}.dat",ind))?;
    for i in 1_usize..(2* NKX as usize) {
        for j in 1_usize..(2* NKX as usize) {
            let mut sum = 0;
            for m in 1_usize..=NX {
                for n in 1_usize..=NX {
                    sum += dist[(m,n,i,j)];
                }
            }
            write!(fp,"{} ",sum)?;
        }
        writeln!(fp)?;
    }

    // saves the two distributions for each particle
    let mut ftot = Array2::<i64>::zeros((NXM+1,NKXM+1));
    // particle #0
    fp = File::create(format!("out/distribution_xk_0_{}.dat",ind))?;
    for i in 1_usize..=NX {
        for m in 1_usize..(2* NKX as usize) {
            let mut sum = 0;
            for j in 1_usize..=NX {
                for n in 1_usize..(2* NKX as usize) {
                    sum += dist[(i,j,m,n)];
                }
            }
            write!(fp, "{} ", sum)?;
            ftot[(i,m)] = sum;
        }
        writeln!(fp)?;
    }
    // particle #1
    fp = File::create(format!("out/distribution_xk_1_{}.dat",ind))?;
    for j in 1_usize..=NX {
        for n in 1_usize..(2* NKX as usize) {
            let mut sum = 0;
            for i in 1_usize..=NX {
                for m in 1_usize..(2* NKX as usize) {
                    sum += dist[(i,j,m,n)];
                }
            }
            write!(fp, "{} ", sum)?;
            ftot[(j,n)] += sum;
        }
        writeln!(fp)?;
    }

    // save densities
    fp = File::create(format!("out/dens_{}.dat",ind))?;
    for i in 1_usize..=NX {
        let mut sum = 0;
        for m in 1_usize..(2* NKX as usize) {
            sum += ftot[(i,m)];
        }
        writeln!(fp, "{0} {1} {2} {3}", (i as f64 - 0.5)*DX, rho[(0,i)], rho[(1,i)], sum)?;
    }

    // saves the induced dipole for the electron
    if ind == 0 {
        for i in 1_usize..=NX {
            rho0[i] = rho[(0,i)];
        }
        fp = File::create("out/induced_dipole.dat")?;
        writeln!(fp, "{0} {1}", ind as f64 * DT * 1.0e15, 0.)?; // in attoseconds
    } else {
        fp = OpenOptions::new().append(true).open("out/induced_dipole.dat")?;
        let mut sum = 0.;
        for i in 1_usize..=NX {
            sum += (i as f64 - 0.5)*(rho[(0,i)]-rho0[i]);
        }
        sum *= DX*DX;
        writeln!(fp, "{} {}",ind as f64 * DT*1.0e15, sum)?;
    }

    Ok(())
}