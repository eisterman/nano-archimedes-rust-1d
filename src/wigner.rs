/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

// calculates the Wigner kernel/potential
// see my manuscript on many-body Wigner MC method.

// This is all we need to calculate the function gamma.

use std::io;
use std::io::Write;

use ndarray::{ArrayViewMut4, ArrayView1};
use ndarray_parallel::prelude::*;

use crate::config::*;

//TODO: La parallelizzazione è più che necessaria per colpa della pesante richiesta di calcolo
//  utilizzando gli Zip del ndarray_parallel
pub fn wigner(mut vw: ArrayViewMut4<f64>, phi: ArrayView1<f64>) {
    println!("\ncalculating Wigner kernel");
    for i in 1_usize..=NX {
        print!("{0}/{1} - ",i,NX);
        io::stdout().flush().unwrap();
        for j in 1_usize..=NX {
            for m in 0_usize..(2* NKX as usize) {
                for n in 0_usize..(2* NKX as usize) {
                    vw[(i,j,m,n)]=0.;
                    for l1 in (((-0.5*LC/DX) as i64)-1)..=(((0.5*LC/DX) as i64)+1) {
                        for l2 in (((-0.5 * LC / DX) as i64) - 1)..=(((0.5 * LC / DX) as i64) + 1) {
                            let ipl1 = i as i64 + l1;
                            let iml1 = i as i64 - l1;
                            let jpl2 = j as i64 + l2;
                            let jml2 = j as i64 - l2;
                            if 1 <= ipl1 && ipl1 <= NX as i64
                                && 1 <= iml1 && iml1 <= NX as i64
                                && 1 <= jpl2 && jpl2 <= NX as i64
                                && 1 <= jml2 && jml2 <= NX as i64 {
                                let arg = 2.*((m as f64-NKX as f64+1.)*DKX*(l1 as f64-0.5)*DX+(n as f64-NKX as f64+1.)*DKX*(l2 as f64-0.5)*DX);
                                vw[(i,j,m,n)] += arg.sin()*(phi[ipl1 as usize]+phi[jpl2 as usize]-phi[iml1 as usize]-phi[jml2 as usize]);
                            }

                        }
                    }
                    vw[(i,j,m,n)] *= 2.*(-Q)*DX*DX/(HBAR*LC*LC);
                }
            }
        }
    }
    println!();
    io::stdout().flush().unwrap();
}
