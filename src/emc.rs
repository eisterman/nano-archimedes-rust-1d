/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::io;
use std::io::Write;

use ndarray::{Array1,ArrayView2,ArrayView4,ArrayViewMut1,ArrayViewMut2};
use ndarray::s;
use rand::Rng;

use std::process::exit;

use crate::config::*;
use crate::utils::rnd;
use crate::NPMAX;

// ensemble Monte Carlo method
// no need to parallelize this routine in 1D space,
// the total number of particles is relatively small.

pub fn emc<T: Rng>(mut updated: ArrayViewMut1<bool>,
                   mut ptime: ArrayViewMut1<f64>,
                   mut inum: &mut i64,
                   mut p: ArrayViewMut2<f64>,
                   mut k: ArrayViewMut2<i64>,
                   gamma: ArrayView2<f64>,
                   mut rng: &mut T,
                   vw: ArrayView4<f64>,
                   mut w: ArrayViewMut1<i64>) {
    // evolution of the particles
    // and creation of (+,-) couples
    let mut i = Array1::<i64>::zeros(2);
    //let mut j = Array1::<i64>::zeros(2);
    let mut x0 = Array1::<f64>::zeros(2);
    let mut k0 = Array1::<f64>::zeros(2);
    let mut hmt = Array1::<f64>::zeros(2);

    println!("\nEvolving particles");
    io::stdout().flush().unwrap();

    // initial settings
    let mut number_of_outside_particles=0;
    let mut all_particles_updated = false;
    updated.slice_mut(s![0..(*inum as usize)]).fill(false);
    ptime.slice_mut(s![0..(*inum as usize)]).fill(DT);

    while all_particles_updated == false {
        let mut number_of_created_particles = 0;
        // evolution and couples creation
        for n in 0..(*inum as usize) {
            hmt[0] = HBAR/ME*ptime[n];
            hmt[1] = HBAR/MP*ptime[n];
            // drift n-th particle
            x0[0] = p[(0,n)];
            x0[1] = p[(1,n)];
            k0[0] = k[(0,n)] as f64*DKX;
            k0[1] = k[(1,n)] as f64*DKX;
            i[0] = (x0[0]/DX) as i64 + 1;
            i[1] = (x0[1]/DX) as i64 + 1;
            // evolve position and wave vector of the n-th particle
            let i_nx = NX as i64;
            let i_nkx = NKX as i64;
            if i[0] > 0 && i[0] <= i_nx && -i_nkx < k[(0, n)] && k[(0, n)] < i_nkx
                && i[1] > 0 && i[1] <= i_nx && -i_nkx < k[(1, n)] && k[(1, n)] < i_nkx {
                p[(0,n)] = x0[0] + hmt[0] * k0[0];
                p[(1,n)] = x0[1] + hmt[1] * k0[1];
                // reflective boundary conditions
                // corresponds to null wave packet on the boundaries
                if p[(0,n)] < 0. {
                    k[(0,n)] *= -1;
                    k0[0] *= -1.;
                    p[(0,n)] = p[(0,n)].abs();
                    x0[0] = p[(0,n)];
                }
                if p[(0,n)] > LX {
                    k[(0,n)] *= -1;
                    k0[0] *= -1.;
                    p[(0,n)] = LX - (p[(0,n)]-LX).abs();
                    x0[0] = p[(0,n)];
                }
                if p[(1,n)] < 0. {
                    k[(1,n)] *= -1;
                    k0[1] *= -1.;
                    p[(1,n)] = p[(1,n)].abs();
                    x0[1] = p[(1,n)];
                }
                if p[(1,n)] > LX {
                    k[(1,n)] *= -1;
                    k0[1] *= -1.;
                    p[(1,n)] = LX - (p[(1,n)]-LX).abs();
                    x0[1] = p[(1,n)];
                }
                // calculates the probability that the wave-vector
                // actually evolves according to the continuous dkx
                // check if a couple of (+,-) have to be created
                let ind_i = i.map(|&x| x as usize);
                if gamma[(ind_i[0],ind_i[1])] != 0. {
                    let mut time = 0.;
                    while time < ptime[n] {
                        let rdt = -rnd(&mut rng).ln()/gamma[(ind_i[0],ind_i[1])];
                        time += rdt;
                        if time < ptime[n] {
                            let mut created = false; // the purpose of this flag is only to make the routine faster
                            let r = rnd(&mut rng);
                            let mut sum = 0.;
                            // random selection of the wave-vector
                            'outer: for j0 in 0_usize..(2*NKX as usize) {
                                for j1 in 0_usize..(2*NKX as usize) {
                                    let p_f64 = vw[(ind_i[0],ind_i[1],j0,j1)]/gamma[(ind_i[0],ind_i[1])];
                                    if sum <= r && r < (sum+p_f64) {
                                        number_of_created_particles += 2;
                                        let mut num = (*inum + number_of_created_particles) as usize;
                                        // select a random time interval when the creation happens
                                        // assign position
                                        p[(0,num-2)] = x0[0]+HBAR/ME*time*k0[0];
                                        p[(0,num-1)] = x0[0]+HBAR/ME*time*k0[0];
                                        p[(1,num-2)] = x0[1]+HBAR/ME*time*k0[1];
                                        p[(1,num-1)] = x0[1]+HBAR/ME*time*k0[1];
                                        // assign wave vector
                                        if vw[(ind_i[0],ind_i[1],j0,j1)] >= 0. {
                                            k[(0,num-2)] = k[(0,n)] + j0 as i64 - NKX as i64 + 1;
                                            k[(0,num-1)] = k[(0,n)] - j0 as i64 + NKX as i64 - 1;
                                            k[(1,num-2)] = k[(1,n)] + j0 as i64 - NKX as i64 + 1;
                                            k[(1,num-1)] = k[(1,n)] - j0 as i64 + NKX as i64 - 1;
                                        } else {
                                            k[(0,num-2)] = k[(0,n)] - j0 as i64 + NKX as i64 - 1;
                                            k[(0,num-1)] = k[(0,n)] + j0 as i64 - NKX as i64 + 1;
                                            k[(1,num-2)] = k[(1,n)] - j0 as i64 + NKX as i64 - 1;
                                            k[(1,num-1)] = k[(1,n)] + j0 as i64 - NKX as i64 + 1;
                                        }
                                        // assign quantum weight
                                        if w[n] == 1 {
                                            w[num-2] = 1;
                                            w[num-1] = -1;
                                        } else {
                                            w[num-2] = -1;
                                            w[num-1] = 1;
                                        }
                                        // assign flag to evolve the particles at the next loop
                                        updated[num-2] = false;
                                        updated[num-1] = false;
                                        // assign time
                                        ptime[num-2] = ptime[n] - time;
                                        ptime[num-1] = ptime[n] - time;
                                        // eventually ignore the just-created couple since
                                        // at least one of them is outside the device
                                        if k[(0,num-2)] <= -i_nkx || k[(0, num-2)] >= i_nkx ||
                                            k[(0,num-1)] <= -i_nkx || k[(0, num-1)] >= i_nkx ||
                                            k[(1,num-2)] <= -i_nkx || k[(1, num-2)] >= i_nkx ||
                                            k[(1,num-1)] <= -i_nkx || k[(1, num-1)] >= i_nkx {
                                            num = *inum as usize -2;
                                            number_of_created_particles -= 2;
                                        }
                                        created = true;
                                    }
                                    sum += p_f64;
                                    if created {
                                        break 'outer;
                                    }
                                }

                                if created {
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                number_of_outside_particles += 1;
            }
            updated[n] = true;
        }
        *inum += number_of_created_particles;
        println!("INUM = {0} -- particles created = {1}",*inum,number_of_created_particles);
        io::stdout().flush().unwrap();
        if *inum as usize > NPMAX {
            println!("Number of particles has exploded - please increase NPMAX and recompile");
            exit(1);
        }
        // check if all particles have been updated
        all_particles_updated = updated.slice(s!(0..(*inum as usize))).iter().all(|x| { *x });
    }
    println!("-- number of particles outside = {0} --",number_of_outside_particles);
    io::stdout().flush().unwrap();
}