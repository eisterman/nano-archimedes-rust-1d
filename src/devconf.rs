/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use num::complex::Complex64;
use num_traits::pow::Pow;
use ndarray::{Array, ArrayViewMut1, ArrayViewMut2, ArrayViewMut4};
use ndarray::{Zip,s};
use ndarray_parallel::prelude::*;
use rand::Rng;

use crate::config::*;
use crate::annihilation::annihilation;
use std::borrow::Borrow;

// initial configuration of electrons in device

pub fn initial_phi(ind: usize, x: f64) -> Complex64 {
    let a1 = Complex64::from(((x-X0_WAVE_PACKET[ind])/SIGMA_WAVE_PACKET[ind]).pow(2.));
    let a = ((-Complex64::i())*0.5*a1).exp();
    let b = (Complex64::i()*K0_WAVE_PACKET[ind]*x).exp();
    a + b
}

pub fn devconf<T: Rng>(inum: &mut i64,
                       mut rng: &mut T,
                       mut dist: ArrayViewMut4<i64>,
                       p: ArrayViewMut2<f64>,
                       k: ArrayViewMut2<i64>,
                       w: ArrayViewMut1<i64>) {
    // double entangled_sigma_x=0.5*SIGMA_WAVE_PACKET[0];
    // double entangled_sigma_k=DKX;
    let inum_f64 = *inum as f64;

    println!("defining the initial distribution function");
    // definition of a wave packet distribution function
    let mut viewer = dist.slice_mut(s![1..=NX,1..=NX,..(2*NKX-1),..(2*NKX-1)]);
    let i = Array::from_shape_fn(viewer.shape(),|indexes| {indexes[0] as f64 + 1.});
    let j = Array::from_shape_fn(viewer.shape(),|indexes| {indexes[1] as f64 + 1.});
    let m = Array::from_shape_fn(viewer.shape(),|indexes| {(indexes[2] as f64)-(NKX as f64)+1.});  // m=-NKX+1;m<NKX
    let n = Array::from_shape_fn(viewer.shape(),|indexes| {(indexes[3] as f64)-(NKX as f64)+1.});  // n=-NKX+1;n<NKX
    let exp = f64::exp; // simplify writing of the expression
    Zip::from(&mut viewer)
        .and_broadcast(&i).and_broadcast(&j).and_broadcast(&m).and_broadcast(&n)
        .par_apply(|x, &i, &j, &m, &n| {
            let a: f64 = (((i-0.5)*DX-X0_WAVE_PACKET[0])/SIGMA_WAVE_PACKET[0]).pow(2);
            let b: f64 = (((j-0.5)*DX-X0_WAVE_PACKET[1])/SIGMA_WAVE_PACKET[1]).pow(2);
            let c: f64 = (((m*DKX)-K0_WAVE_PACKET[0])*SIGMA_WAVE_PACKET[0]).pow(2);
            let d: f64 = (((n*DKX)-K0_WAVE_PACKET[1])*SIGMA_WAVE_PACKET[1]).pow(2);
            *x = (inum_f64*exp(-a)*exp(-b)*exp(-c)*exp(-d)) as i64;
        });
    annihilation(inum,rng, dist, p, k, w);

    println!("Initial Number of Electron Super-particles = {0}", *inum)
}