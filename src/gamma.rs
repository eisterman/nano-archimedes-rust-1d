/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::io;
use std::io::Write;

use ndarray::{ArrayView4,ArrayViewMut2};
use ndarray_parallel::prelude::*;

use crate::config::*;

pub fn calculate_gamma(mut gamma: ArrayViewMut2<f64>, vw: ArrayView4<f64>) {
    println!("\ncalculating the gamma function");

    for i in 1_usize..=NX {
        print!("{0}/{1} - ",i,NX);
        io::stdout().flush().unwrap();
        for j in 1_usize..=NX {
            gamma[(i,j)] = 0.;
            for m in 0..(2* NKX as usize) {
                for n in 0..(2* NKX as usize) {
                    // use VW+
                    if vw[(i,j,m,n)] > 0. {
                        gamma[(i,j)] += vw[(i,j,m,n)];
                    }
                }
            }

        }
    }
    println!();
    io::stdout().flush().unwrap();
}