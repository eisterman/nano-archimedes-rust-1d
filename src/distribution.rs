/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use std::io;
use std::io::Write;

use ndarray::{ArrayView1,ArrayView2,ArrayViewMut4};
use ndarray_parallel::prelude::*;

use crate::config::*;

// calculates the distribution function according to the quantum weights

pub fn distribution(inum: i64,
                    mut dist: ArrayViewMut4<i64>,
                    p: ArrayView2<f64>,
                    k: ArrayView2<i64>,
                    w: ArrayView1<i64>) {
    println!("\nCalculation of distribution function");
    println!("Number of particles = {0}", inum);
    io::stdout().flush().unwrap();

    // set the distribution function to zero
    dist.fill(0);

    for n in 0..inum as usize {
        let i_nx = NX as i64;
        let i_nkx = NKX as i64;
        let i0 = (p[(0,n)]/DX) as i64 +1;
        let i1 = (p[(1,n)]/DX) as i64 +1;
        let k0 = k[(0,n)];
        let k1 = k[(1,n)];
        if 0 < i0 && i0 <= i_nx && -i_nkx < k0 && k0 < i_nkx &&
            0 < i1 && i1 <= i_nx && -i_nkx < k1 && k1 < i_nkx {
            let i0 = i0 as usize;
            let i1 = i1 as usize;
            let k0 = k0 as usize;
            let k1 = k1 as usize;
            let u_nkx = NKX as usize;
            dist[(i0,i1,k0+u_nkx-1,k1+u_nkx-1)] += w[n];
        }
    }

    println!("end of distribution function calculation");
    io::stdout().flush().unwrap();
}
