/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use ndarray::{Axis,ArrayViewMut2,ArrayView4};
use ndarray::{s,Zip,azip};
use ndarray_parallel::prelude::*;

use crate::config::*;

pub fn density(dist: ArrayView4<i64>,
               mut rho: ArrayViewMut2<f64>) {
    rho.fill(0.);  // reset the density array

    // calculation of the normalized density
    println!("\ncalculation of the particle densities");
    for i in 0..=NX {
        for j in 0..=NX {
            for m in 0..2*(NKX-1) as usize {  // m=-NKX+1;m<NKX;m++
                for n in 0..2*(NKX-1) as usize {
                    rho[(0,i)] += dist[(i,j,m,n)] as f64;
                    rho[(1,j)] += dist[(i,j,m,n)] as f64;
                }
            }
        }
    }

    // normalization of the density
    for l in 0..2 {
        let mut sum = 0.;
        for i in 1..=NX {
            sum += rho[(l,i)];
        }
        sum *= DX;
        //TODO: Parallelizzare e usare gli algoritmi di ndarray
        for i in 1..=NX {
            rho[(l,i)] /= sum;
        }
    }
}