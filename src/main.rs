/* ===============================================================
nano-archimedes-rust-1D
Copyright (C) 2004-2015  Jean Michel D. Sellier <jeanmichel.sellier@gmail.com>
Copyright (C) 2019       Federico Pasqua <federico.pasqua.96@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=============================================================== */

use rand::prelude::*;
use ndarray::{Array1,Array2,Array4};
use ndarray::s;
use chrono::{DateTime, Local};
use std::fs::create_dir_all;
use std::io;
use std::io::Write;

const NPMAX: usize = 32000000; // maximum number of super-particles

const MN3: usize = 3;

mod config;
use config::*;

mod utils;
use utils::rnd;

mod devconf;
use devconf::devconf;

mod annihilation;
use annihilation::annihilation;

mod density;
use density::density;

mod save;
use save::save;

mod hartree;
use hartree::hartree;

mod wigner;
use wigner::wigner;

mod gamma;
use gamma::calculate_gamma;

mod emc;
use emc::emc;

mod distribution;
use distribution::distribution;

fn main() {
    let mut inum: i64 = 1024;

    // define random numbers generator
    const ISEED: u64 = 38467;
    let mut rng = rand::rngs::StdRng::seed_from_u64(ISEED);

    print!("\n\
===========================\n\
 K = [0.:{0}:{1}] 1/m\n\
===========================\n",DKX,(NKX as f64)*DKX);

    // print the initial number of particles
    println!("\nMAXIMUM NUMBER OF PARTICLES ALLOWED = {0}\n",NPMAX);

    // memory allocation
    println!("allocating memory");
    let shape4 = (NX + 1, NX + 1, 2 * NKX as usize + 1, 2 * NKX as usize + 1);
    let mut vw = Array4::<f64>::zeros(shape4);
    let mut dist = Array4::<i64>::zeros(shape4);
    let mut gamma = Array2::<f64>::zeros((NX + 1, NX + 1));
    let mut k = Array2::<i64>::zeros((2, NPMAX + 1));
    let mut w = Array1::<i64>::zeros(NPMAX + 1);
    let mut updated = Array1::<bool>::from_elem(NPMAX + 1, false);
    let mut p = Array2::<f64>::zeros((2, NPMAX + 1));
    let mut ptime = Array1::<f64>::zeros(NPMAX + 1);
    let mut phi = Array1::<f64>::zeros(NXM + 1);
    let mut phi0 = Array1::<f64>::zeros(NXM + 1);
    let mut e = Array1::<f64>::zeros(NXM + 1);
    let mut rho = Array2::<f64>::zeros((2, NXM + 1));
    let mut rho0 = Array1::<f64>::zeros((NXM + 1));
    println!("All memory has been allocated successfully.\n");

    // constant electrostatic potential (included in phi and phi0 definition)
    // gamma function already at zero

    // get initial time
    let now: DateTime<Local> = Local::now();
    println!("simulation started   : {}", now);

    // initialization
    devconf(&mut inum,
            &mut rng,
            dist.view_mut(),
            p.view_mut(),
            k.view_mut(),
            w.view_mut());  // device configuration - distributes particles in the device
    density(dist.view(),
            rho.view_mut());
    create_dir_all("out").expect("out folder error"); // ensure out folder exist
    save(0, phi.view(),
         gamma.view(),
         dist.view(),
         rho.view(),
         rho0.view_mut()).expect("Saving Error!");

    println!();
    let mut time = 0.;
    // updates the solution
    for i in 1_i64..=ITMAX {
        time += DT;
        println!("{0} of {1} -- TIME={2}\n",i,ITMAX,time);
        io::stdout().flush().unwrap();
        density(dist.view(),
                rho.view_mut());
        if (i % 50) == 0 {
            hartree(rho.view(),
                    phi.view_mut(),
                    phi0.view());
            wigner(vw.view_mut(),
                   phi.view());
            calculate_gamma(gamma.view_mut(),
                            vw.view());
        }
        emc(updated.view_mut(),
            ptime.view_mut(),
             &mut inum,
            p.view_mut(),
            k.view_mut(),
            gamma.view(),
             &mut rng,
            vw.view(),
            w.view_mut());
        distribution(inum,
                     dist.view_mut(),
                     p.view(),
                     k.view(),
                     w.view());
        if (i % 10) == 0 {
            println!("Annihilation of particles");
            if i != ITMAX {
                annihilation(&mut inum,
                             &mut rng,
                             dist.view_mut(),
                             p.view_mut(),
                             k.view_mut(),
                             w.view_mut());
            }
        }
        save(i, phi.view(),
             gamma.view(),
             dist.view(),
             rho.view(),
             rho0.view_mut()).expect("Saving Error!");
    }
    println!();

    println!("output files saved\n");

    // get final time and exit.
    let now: DateTime<Local> = Local::now();
    println!("simulation ended     : {}", now);
}
